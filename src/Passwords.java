import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.RandomAccessFile;
import java.io.Reader;

public class Passwords {
	
	public static void main (String[] args) throws IOException{
		int[] COUNTS = new int[26];
		int[] STARTERS = new int[26];
		int[][] followers = new int [26][26];
		
		String test2 = args[0];
		File text = new File(test2);
		//Scanner sc = new Scanner(text);
		int n = Integer.parseInt(args[1]);
		int k = Integer.parseInt(args[2]);
		
		//int n = 10;
		//int k = 10;
		
		//byte letter = sc.nextByte();
		InputStream in = new FileInputStream(text);
        @SuppressWarnings("resource")
		Reader reader = new InputStreamReader(in);
        // buffer for efficiency
        // Reader buffer = new BufferedReader(reader);
        
        char ch = 0;
        boolean space = true;
	
		int a;
		int b;
		@SuppressWarnings("resource")
		RandomAccessFile file = new RandomAccessFile((text), "r");
		long index, length;
		length = file.length() - 1; 
		for (index = 0; index < length; index++) {
		    a = reader.read();
			file.seek(index+1);
			b = file.read();
		    
			if (a >= 65 && a <= 90){
				a += 32;
			}
			if (b >= 65 && b <= 90){
				b += 32;
			}
			if(a == 10 || a == 32){
				space = true;
			}
			
			
//COUNTS IMPLEMENTATION
			if(a >= 97 && a <= 122){
				if(b == 10 || b == 32)
				{}
				else{
				//97 is the lowercase a ascii value
					COUNTS[a-97]++;
				}
			}
			
//STARTERS IMPLEMENTATION
			if(a >= 97 && a <= 122 && space){
				//97 is the lowercase a ascii value
				STARTERS[a-97]++;
				space = false;
			}
	
			//System.out.print("(" + ch_a + "-" + a + "," + ch_b + "-" + b +")" + " ");
//followers table			
			if(a >= 97 && a <= 122)
			{
				if((b >= 97 && b <= 122)){
					followers[a - 97][b - 97]++;
				}
			}
		}
/*
		for(int i = 0; i < 26; i++){
			ch = (char)(i + 97);
			System.out.print(ch + "-" + COUNTS[i] + " ");
		}
		
		System.out.println();
*/
		
		int total = 0;
		for(int i = 0; i < 26; i++){
			ch = (char)(i + 97);
			total+=STARTERS[i];
			//System.out.print(ch + "-" + STARTERS[i] + " ");
		}
/*
		System.out.println();
		System.out.println();
		
		System.out.print("  ");
		for(int i = 0; i < 26; i++){
			ch = (char) ((char)i+65);
			System.out.printf("%3c ", ch);
		}

		for(int i = 0; i < 26; i++)
		{
			System.out.println();
			ch = (char) ((char)i+65);
			System.out.print(ch + ":");
			for(int j = 0; j < 26; j++)
			{
				System.out.printf("%3d ", followers[i][j]);
			}
		}
		
		System.out.println();
		System.out.println(total);
*/
		int num = 0;
		char ch2 = 0;
		boolean first = true;
		int nextLetter = 0;
		int secondLetter = 0;
		
		//number of passwords wanted
		System.out.println("Passwords are: ");
		for(int i = 0; i < n; i++)
		{
			int firstLetter = 0;
			first = true;
			num = (int) (Math.random() * (total+1 - 0)) + 0;
			//System.out.println(num + " ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
			int leng = 0;
			//how long the user wants the password
			for(int f = 0; f < k; f++)
			{
				for(int j = 0; j < 26; j++)
				{
					if(leng == k)
						break;
					//System.out.print(" LENG: " + leng + " K: " + k + " J: " + j + " ");
					firstLetter += STARTERS[j];
					if((firstLetter >= num) && first)
					{
						leng++;
						ch = (char)(j+97);
						nextLetter = j;
						first = false;
						System.out.print(ch);
					}
					ch = (char)(nextLetter+97);
				
					if(first == false)
					{	
						leng++;
						//second letter
						secondLetter = 0;
						int rowSum = 0;
						//char nnn = (char)(nextLetter + 97);
						//System.out.println("NEXT LETTER: " + (nnn));
						for(int z = 0; z < 26; z++)
						{
							rowSum+=followers[nextLetter][z];
						}
						num = (int) (Math.random() * (rowSum +1 - 0)) + 0;
						//System.out.println(num + " ~~~~ " + rowSum);
					
						for(int l = 0; l < 26; l++)
						{
							secondLetter += followers[nextLetter][l];
							//System.out.print(" $$$ " + followers[nextLetter][l]);
						
							if(secondLetter >= num)
							{
								//System.out.print(" PF " + prevFirst + " OC " + l);
								ch2 = (char)(l+97);
								System.out.print(ch2);
								nextLetter = l;
								break;
							}
						}
					}
				}
			}
			System.out.println();
		}
		System.out.println();
	}
}
