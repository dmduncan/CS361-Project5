UTEID: eab2982; dmd2479; dic223;
FIRSTNAME: Eddie; Daniel; Dylan;
LASTNAME: Babbe; Duncan; Inglis;
CSACCOUNT: dmduncan; babbe99; cinglis;
EMAIL: daniel.duncan@utexas.edu; babbe2012@utexas.edu; dylanmcquade@yahoo.com;

Note: Professor Young has given us permission to work as a team of 3. You can confirm with him if needed.

[Program 5] [Description]
There was really only a need to make 1 file for this assignment, which was Passwords.java. There were 3 arrays used in this program. 2 1D arrays to keep track of the number of occurrences of a letter in the text and then another for the number of occurrences of a letter at the beginning of a word in the text. Then I made a 2D array to record the amount of times a specific letter follows a given letter. With that, when computing random passwords you randomly generate a random number between 0 and the total number of counts of letters at the beginning of a word. Then with that number go through the 1D array called starters adding up each consecutive count until you are equal or greater than the random number, whatever index you are now on is the letter you start with. Once you have the letter to start with you go to that row in the 2D follows array and compute another random number between 0 and the total counts of the row. Then iterate down it and whenever you get the same number or greater than the random computed number that it the next letter following. Then you make that letter the 1st letter and repeat the process for however long you want the password to be. 

[Finish] We have completely finished the assignment.

[Source of Reference File]
The text file was some random notes I found on my computer I seemed to have typed in office hours or during class. It is called text and is located inside src of CS361-Project5. I just created a new text file called text and pasted some random notes I took on my computer in there. There are 210 words in the text fileSize of this file. 

[[Test Cases]
[Input of test 1]
java Passwords text 15 9

[Output of test 1]

Passwords are: 
wangother
ncequasth
stysohypr
hyondffen
theathatu
atwalwath
teredfere
ithiserer
dinuathan
anemeresh
olaypogre
ingerreth
bedissoiv
lesenyals
idisethed
   
[Input of test 2]
java Passwords text 10 10

[Output of test 2]

Passwords are: 
compothere
thegessess
nsypreranc
urorarsesi
lotouanyav
cesothetha
tsequsemes
conthalldi
therespres
thypthatha

[Input of test 3]
java Passwords text 20 20

[Output of test 3]

Passwords are: 
zasiongatosseanserso
thesinuasedisovessth
thatotifersingerewhe
herereroullothyosisi
nssenuluthalotyolles
teretheratulofeteren
tiprcongasequtherero
haffrendoverthatinth
syprondfeenongathanu
aschesoffeassisealut
opthencithewethegaso
thyprofallayprorequa
pthereraypoullasedan
inthedinypisetequayp
atealllllotherngrome
sitatheropanthangava
therousterongrthated
vesisisisitsopoosisa
isthinuldffffferotha
isewhetingreangolart

[Input of test 4]
java Passwords text 15 40

[Output of test 4

Passwords are: 
southereangrerenaypremifanysinarthangoth
tenetisorreredidanyposhetothantissegroat
theaserrthatherevereereredinatovisthesty
dfenarovespreatopupononereathisosewnisys
oissisesulwarerofedaserequlwitoreresotwh
doneroopathreshessoiffffresetyomesisiaya
theshyprolweereqatosethedatoteltaveetese
wanspohasofranifetheesedissorovevewedise
thequathessenongrthenoweditareresitiveit
insathypotertesisypotereditisthypohabuay
spusequtoulllyarthrarewalaveatherediotha
andfferemetheilareditheveanceroviovarena
waltivethrescayontonsivewilempoallwaypal
bllsoressemrthedisisalenthererovathaldis
nutyavenusinendissetonuafedfetitogonurti
